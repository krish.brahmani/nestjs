import { DynamicModule, Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';
import { FormModule } from 'src/module/form/form.module';
import { PostModule } from 'src/module/post/post.module';
import { UserModule } from 'src/module/user/user.module';
const dynamicModule = [
    {
        path: 'user',
        module: UserModule,
    },
    {
        path: 'form',
        module: FormModule,
    },
    {
        path: 'post',
        module: PostModule,
    },
];

@Module({})
export class AppRouterModule {
    static register(): DynamicModule {
        return {
            module: AppRouterModule,
            imports: [
                ...dynamicModule.map((item) => item.module),
                RouterModule.register(dynamicModule),
            ],
        };
    }
}
