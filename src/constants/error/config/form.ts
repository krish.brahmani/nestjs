import { HttpStatus } from '@nestjs/common';
import { ErrorConfig } from '../error.types';
import { FORM_ERROR } from '../errors/form';

export const formErrorConfig: ErrorConfig<FORM_ERROR> = {
    [FORM_ERROR.FORM_NOT_EXIST]: {
        message: 'Form not exist',
        statusCode: HttpStatus.BAD_REQUEST,
        errorCode: 'FORM_NOT_EXIST_ERROR',
    },
    [FORM_ERROR.FORM_TITLE_EXIST]: {
        message: 'title already exists',
        statusCode: HttpStatus.BAD_REQUEST,
        errorCode: 'FORM_TITLE_EXIST_ERROR',
    },
};
