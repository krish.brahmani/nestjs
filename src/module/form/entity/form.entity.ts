import { Column, DataType, HasMany, Model, Table } from 'sequelize-typescript';
import { FormDTO } from '../dto/form.dto';
@Table({ tableName: 'form' })
export class FormEntity extends Model<FormDTO> {
    @Column({
        primaryKey: true,
        type: DataType.UUID,
        defaultValue: DataType.UUIDV4,
    })
    uniqueId: string;

    @Column({
        allowNull: false,
        type: DataType.STRING,
    })
    title: string;

    @Column({
        allowNull: false,
        type: DataType.STRING,
    })
    name: string;

    @Column({
        allowNull: false,
        type: DataType.STRING
    })
    email: string;

    @Column({
        allowNull: false,
        type: DataType.STRING,
    })
    phonenumber: string;

    @Column({
        allowNull: false,
        type: DataType.STRING,
    })
    IsGraduate: string;
}
