import { Body, Controller, Get, Headers, Param, Post } from '@nestjs/common';
import { AuthService } from 'src/service-gateway/service/Auth.service';
import { responseName } from '../../../constants/response';
import { Response as ResponseCustom } from '../../../utils/response/response.decorator';
import { CreateFormDTO } from '../dto/form.dto';
import { FormService } from '../service/form.service';

@Controller()
export class FormController {
    constructor(
        private formService: FormService,
        private authService: AuthService,
    ) {}

    @Post()
    @ResponseCustom(responseName.FORM_CREATED)
    async createForm(@Body() createFormDTO: CreateFormDTO) {
        console.log(createFormDTO);
        return await this.formService.createForm(createFormDTO);
    }

    @Get()
    @ResponseCustom(responseName.GET_ALL_USERS)
    async getAllUser() {
        return await this.formService.getAllUser();
    }

    @Get('/getAuthToken')
    async getAuthToken(@Headers('Authorization') header: string) {
        const token = header;
        return { token };
    }

    @Get('/callToMicroservice')
    async callAuth() {
        const result = this.authService.getAuthToken();
        return result;
    }
}
