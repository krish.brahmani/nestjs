import { Test } from '@nestjs/testing';
import { FormController } from '../controller/form.controller';
import { FormService } from '../service/form.service';

describe('Forms Controllers', () => {
    let formService: FormService;
    let formController: FormController;

    const data = {
        name: 'userName',
        email: 'user@yopmail.com',
        designation: 'dev',
    };

    const mockUserService = {
        createForm: jest.fn().mockReturnValue(data),
        getAllForm: jest.fn().mockReturnValue([data]),
        findAl: jest.fn().mockReturnValue([data]),
        findOne: jest.fn().mockReturnValue(data),
        update: jest.fn().mockReturnValue(data),
        remove: jest.fn().mockReturnValue(data),
    };

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            controllers: [FormController],
            providers: [
                {
                    provide: FormService,
                    useValue: mockUserService,
                },
            ],
        }).compile();

        formService = moduleRef.get<FormService>(FormService);
        formController = moduleRef.get<FormController>(FormController);
    });

    describe('user Repository create', () => {
        it('should return a promise', async () => {
            const req = {
                title:"Test form",
                name: 'userName',
                email: 'user@yopmail.com',
                designation: 'dev',
                phonenumber:'1231231234',
                IsGraduate:true
            };
            expect(formController.createForm(req)).resolves;
            expect(formService.createForm).toHaveBeenCalledTimes(1);
            expect(formService.createForm).toHaveBeenCalledWith(req);
        });
    });

    describe('user Repository getAllUser', () => {
        it('should return a promise', async () => {
            expect(formController.getAllUser()).resolves;
            expect(formService.getAllUser).toHaveBeenCalledTimes(1);
            expect(formService.getAllUser).toHaveBeenCalledWith();
        });
    });
});
