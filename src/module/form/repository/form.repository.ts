import { Injectable } from '@nestjs/common';
import { PostEntity } from '../../post/entity/post.entity';
import { CreateFormDTO } from '../dto/form.dto';
import { FormEntity } from '../entity/form.entity';

@Injectable()
export class FormRepository {
    async create(createFormDTO: CreateFormDTO) {
        return await FormEntity.create(createFormDTO);
    }

    async getAll() {
        return await FormEntity.findAndCountAll();
    }

    async getById(id: string) {
        return await FormEntity.findByPk(id, {
            include: [
                {
                    model: PostEntity,
                    attributes: { exclude: ['userId'] },
                },
            ],
        });
    }

    async getByTitle(title: string) {
        return await FormEntity.findOne({
            where: { title },
        });
    }
}
