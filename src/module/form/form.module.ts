import { Module } from '@nestjs/common';
import { SearchModule } from 'src/search/search.module';
import { ServiceGatewayModule } from 'src/service-gateway/serviceGateway.module';
import { FormController } from './controller/form.controller';
import { FormRepository } from './repository/form.repository';
import { FormService } from './service/form.service';

@Module({
    imports: [ServiceGatewayModule, SearchModule],
    controllers: [FormController],
    providers: [FormService, FormRepository],
    exports: [FormService],
})
export class FormModule {}
