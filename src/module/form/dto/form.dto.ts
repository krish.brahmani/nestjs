import { ApiProperty, OmitType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
    IsNotEmpty,
    IsString,
    IsUUID,
} from 'class-validator';


export class FormDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsUUID(4)
    uniqueId: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    title: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    email: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    phonenumber: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    IsGraduate: string;
}

export class CreateFormDTO extends OmitType(FormDTO, ['uniqueId'] as const) {}

export class GetUserPathParams {
    @IsNotEmpty()
    @IsUUID(4)
    uniqueId: string;
}
