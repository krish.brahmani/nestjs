import { Injectable } from '@nestjs/common';
import { FORM_ERROR } from '../../../constants/error';
import { HttpExceptionWrapper } from '../../../utils/error/error.http.wrapper';
import { CreateFormDTO } from '../dto/form.dto';
import { FormRepository } from '../repository/form.repository';

@Injectable()
export class FormService {
    constructor(private formRepository: FormRepository) {}

    async createForm(createFormDTO: CreateFormDTO) {
        const form = await this.formRepository.getByTitle(createFormDTO?.title);
        if (form) {
            throw new HttpExceptionWrapper(FORM_ERROR.FORM_TITLE_EXIST);
        }

        console.log(JSON.stringify(createFormDTO));
        const createdUser = await this.formRepository.create(createFormDTO);
        console.log(JSON.stringify(createdUser));
        return createdUser.toJSON();
    }

    async getAllUser() {
        const users = await this.formRepository.getAll();
        const rows = users.rows;
        const count = users.count;

        return {
            usersCount: count,
            users: rows,
        };
    }

    async getByTitle(title: string) {
        const form = await this.formRepository.getByTitle(title);
        return form;
    }
}
 