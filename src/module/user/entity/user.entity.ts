import {
    Column,
    DataType,
    ForeignKey,
    Model,
    Table,
} from 'sequelize-typescript';
import { UserDTO } from '../dto/user.dto';
import { FormEntity } from 'src/module/form/entity/form.entity';
@Table({ tableName: 'user' })
export class UserEntity extends Model<UserDTO> {
    @Column({
        primaryKey: true,
        type: DataType.UUID,
        defaultValue: DataType.UUIDV4,
    })
    uniqueId: string;

    @ForeignKey(() => FormEntity)
    @Column({
        allowNull: false,
        type: DataType.STRING,
    })
    title: string;

    @Column({
        allowNull: false,
        type: DataType.STRING,
    })
    name: string;

    @Column({
        allowNull: false,
        type: DataType.STRING,
        unique: true,
    })
    email: string;

    @Column({
        allowNull: false,
        type: DataType.INTEGER,
    })
    phonenumber: number;

    @Column({
        defaultValue: true,
        type: DataType.BOOLEAN,
    })
    IsGaradute: boolean;
}
