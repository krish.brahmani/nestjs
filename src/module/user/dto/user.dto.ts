import { ApiProperty, OmitType } from '@nestjs/swagger';
import { Expose,Type } from 'class-transformer';

import {
    IsBoolean,
    IsEmail,
    IsNotEmpty,
    IsNumber,
    IsOptional,
    IsString,
    IsUUID,
} from 'class-validator';

export class UserDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsUUID(4)
    uniqueId: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsOptional()
    @IsString()
    title: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @ApiProperty()
    @IsNotEmpty()
    @Type(() => Number)
    @IsNumber()
    phonenumber: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsBoolean()
    IsGraduate: boolean;
}

export class CreateUserDTO extends OmitType(UserDTO, ['uniqueId'] as const) {}

export class GetUserPathParams {
    @IsNotEmpty()
    @IsUUID(4)
    uniqueId: string;
}

export class CreateUserDTO1 {
    @Expose()
    title: string;

    @Expose()
    name: string;

    @Expose()
    email: string;

    @Expose()
    phonenumber: number;

    @Expose()
    IsGraduate: boolean; // Assuming this property is available in CreateUserDTO

    constructor(title: string, name: string, email: string, phonenumber: number, IsGraduate: boolean) {
        this.title = title;
        this.name = name;
        this.email = email;
        this.phonenumber = phonenumber;
        this.IsGraduate = IsGraduate;
    }
}
