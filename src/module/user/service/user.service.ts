import { Injectable } from '@nestjs/common';
import { SearchService } from 'src/search/search.service';
import { AzureKeyVaultService } from '../../../azure-key-vault/azureKeyVault.service';
import { FORM_ERROR, USER_ERROR } from '../../../constants/error';
import { RedisService } from '../../../redis/redis.service';
import { HttpExceptionWrapper } from '../../../utils/error/error.http.wrapper';
import { CreateUserDTO } from '../dto/user.dto';
import { UserRepository } from '../repository/user.repository';
import { FormService } from 'src/module/form/service/form.service';


@Injectable()
export class UserService {
    constructor(
        private userRepository: UserRepository,
        private formService: FormService,
        private redisService: RedisService,
    ) {}

    async createUser(createUserDTO: CreateUserDTO) {
        const form = await this.formService.getByTitle(createUserDTO?.title);
        if (!form) {
            throw new HttpExceptionWrapper(FORM_ERROR.FORM_NOT_EXIST);
        }

        const user = await this.userRepository.getByEmail(createUserDTO?.email);
        if (user) {
            throw new HttpExceptionWrapper(USER_ERROR.USER_EMAIL_EXIST);
        }

        const createdUser = await this.userRepository.create(createUserDTO);
        console.log(JSON.stringify(createdUser));
        return createdUser.toJSON();
    }

    async getAllUser(formTitle: any) {
        const users = await this.userRepository.getAll(formTitle);
        const rows = users.rows;
        const count = users.count;

        return {
            usersCount: count,
            users: rows,
        };
    }
    async getAllUserFromDb2() {
        const users = await this.userRepository.getAllFromDb2();
        const rows = users.rows;
        const count = users.count;

        return {
            usersCount: count,
            users: rows,
        };
    }

    async getUserById(id: string) {
        const user = await this.redisService.getValue(`user${id}`);
        if (!user) {
            console.log('getting from db');
            const user = await this.userRepository.getById(id);
            if (!user) {
                throw new HttpExceptionWrapper(USER_ERROR.USER_NOT_EXIST);
            }
            await this.redisService.setValue(`user${id}`, user, false, 5000);
            return user.toJSON();
        }
        console.log('getting from redis');
        return user;
    }
}
